const splitting = () => {
  let bill = parseInt(document.querySelector('#bill').value);
  let friends = parseInt(document.querySelector('#friends').value);
  
    const result = bill / friends;
    document.querySelector('#share').innerHTML = result.toFixed(2);
  }
 
const splitBtn = document.querySelector('#splitBtn')
  splitBtn.addEventListener('click', (e) => {
    e.preventDefault();
    splitting();
})